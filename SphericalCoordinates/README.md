### Spherical Coordinates ###

This example demonstrates how to create a Transform-Feedback based particle system that moves around a sphere.
It's built off of the [ParticleSphereGPU example](https://github.com/cinder/Cinder/tree/master/samples/_opengl/ParticleSphereGPU) and adapted from this [Processing tutorial by Jer THorp](http://blog.blprnt.com/blog/blprnt/processing-tutorial-spherical-coordinates)


There are some minor changes and notes 

* For some reason, the system always seems to gravitate towards the center every so often. 
* It probabbly won't look so hot with more that 100,000 particles. 
* Some of the old variables are left for comparison. 