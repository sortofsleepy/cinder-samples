#version 150 core

uniform float uMouseForce;
uniform vec3  uMousePos;

in vec3   iPosition;
in vec3   iPPostion;
in vec3   iHome;
in vec4   iColor;
in float iTheta;
in float iPhi;
in float iThetaSpeed;
in float iPhiSpeed;

out vec3  position;
out vec3  pposition;
out vec3  home;
out vec4  color;
out float theta;
out float phi;
out float thetaSpeed;
out float phiSpeed;

const float dt2 = 1.0 / (60.0 * 60.0);
void main()
{
    position =  iPosition;
    pposition = iPPostion;
    home =      iHome;
    color =     iColor;
    theta = iTheta;
    phi = iPhi;
    thetaSpeed = iThetaSpeed;
    phiSpeed = iPhiSpeed;
    
    
    theta += thetaSpeed * 0.05;
    phi += phiSpeed * 0.05;
    
    //radius of the sphere you want the particles to orbit around.
    // TODO make into unifom later
    float r = 400;
    
    //Convert spherical coordinates into Cartesian coordinates
    position.x = cos(theta) * sin(phi) * r;
    position.y = sin(theta) * sin(phi) * r;
    position.z = cos(phi) * r;
    
    
}



