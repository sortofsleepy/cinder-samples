//
//  Terrain.hpp
//  ProtoTerrain
//
//  Created by Joseph Chow on 2/19/16.
//
//

#ifndef Terrain_hpp
#define Terrain_hpp


class Terrain {
    
    ci::gl::VboMeshRef mesh;
    ci::gl::GlslProgRef shader;
    ci::gl::BatchRef mBatch;
    
    ci::gl::VboMeshRef terrain;
    
    //! noisebuffer to generate a constantly updating noise texture
    ci::gl::FboRef noiseBuffer;
    
    //! shader to handler noise generation
    ci::gl::GlslProgRef noiseShader;
    
    uint32_t mOffset;
    
    // width and height of our mesh
    static const int kWidth = 1024;
    static const int kHeight = 1024;
    
    // number of frequency bands of our spectrum
    static const int kBands = 1024;
    static const int kHistory = 128;
public:
    Terrain();
    void updateNoise();
    void buildMesh();
    void draw();
};

#endif /* Terrain_hpp */
