#version 150
uniform vec3 eyePos;
uniform mat4 ciModelViewProjection;
out float fogAmount;
in vec3 ciPosition;
in vec3 ciNormal;
in vec2 ciTexCoord0;

out vec2 vUv;
out vec3 vNormal;
out vec3 vPosition;
out vec3 vEyeDir;


#define FOG_START 90
#define FOG_END 200
float fogFactorLinear(
  const float dist,
  const float start,
  const float end
) {
  return 1.0 - clamp((end - dist) / (end - start), 0.0, 1.0);
}
void main(){
    vUv = ciTexCoord0;
    vNormal = ciNormal;

    gl_Position = ciModelViewProjection * vec4(ciPosition,1.);
    
    vPosition = ciPosition;
    vPosition *= 40.0;
    vEyeDir = normalize(eyePos - vPosition);
    
    float fogDistance = length(gl_Position.xyz);
    fogAmount = fogFactorLinear(fogDistance,FOG_START,FOG_END);
}