__Slit Scan Tests__

This demonstrates a basic example of slit scanning by porting [Golan Levin's original example.](http://www.flong.com/storage/images/texts/slit_scan/processing/basic_slit_scan_live.pde)
It does differ a little bit in that I decided to try looping over the full 
Capture object area instead of only the y axis.

Note that Golan's example uses Processing. In Processing, you can capture the current status of all of the pixels in the window through the `pixels[]` array, which is used in the example. Since there isn't a similiar array in Cinder, the code includes a simple calculation to determin the x/y coordinates of the area based on a index which is calculated in the example. 
