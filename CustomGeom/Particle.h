//
//  ParticleGeom.h
//  InstancedFeedback
//
//  Created by Joseph Chow on 10/24/15.
//
//

#ifndef __InstancedFeedback__ParticleGeom__
#define __InstancedFeedback__ParticleGeom__

#include "cinder/Vector.h"
#include "cinder/gl/Batch.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"

using namespace ci;
using namespace ci::geom;

class ParticleGeom : public ci::geom::Source {

public:
    ParticleGeom();
    
    //! not required - just here for example purposes.
    ParticleGeom&		setScale( float scale );
    
    
    //from here - these are all required functions
    size_t		getNumVertices() const override {return 3;}
    size_t		getNumIndices() const override { return 0; }
    Primitive	getPrimitive() const override { return Primitive::TRIANGLE_FAN; }
    uint8_t		getAttribDims( Attrib attr ) const override;
    AttribSet	getAvailableAttribs() const override;
    void		loadInto( Target *target, const AttribSet &requestedAttribs ) const override;
    ParticleGeom*		clone() const override { return new ParticleGeom( *this ); }
    
private:
    float scale = 1.0;

    size_t		mNumVertices;
};


#endif /* defined(__InstancedFeedback__ParticleGeom__) */
