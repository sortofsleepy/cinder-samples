layered-postprocessing
===

Simple example showing how to layer items using an FBO and how one might selectively post-process certain layers.

It's a bit messy right now, will go back at some point and remove the extraneous code but the most important piece of the puzzel is
the layering which is handled by `FxPass.hpp`. This class manages an FBO for each layer.

The process to layer each object of the scene is essentially
* make a new `FxPass` instance. 
* in between the instance's `bind()` and `unbind()` functions, draw your scene
* By default, will do a pass-through of things when calling `draw()`, but if you setup a fragment shader by calling `setShader()`, it'll pass through the second layer of the 
FBO and run your scene through that fragment shader. You can then see the result by calling `draw(1)`
* You can layer the resulting textures by enabling additive blending
