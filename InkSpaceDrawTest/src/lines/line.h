//
//  lineUtilities
//  DrawTests
//  Pretty much just a straight port of Lieberman's stuff. cinder::Timer added to keep track of delta time.
//  https://github.com/ofZach/inkSpace
//  Created by Joseph Chow on 3/12/16.
//
//

#pragma once
#include "MathUtils.h"
#include "cinder/Timer.h"

using namespace ci;
using namespace std;
using namespace ci::app;
#include "PLine.hpp"
class lineWithInfo {
    
    ci::Timer mTimer;
public:

	lineWithInfo(){
		bAmDoneDrawing = false;
        
	}



	int startTimeMillis;
	int durationMillis;
	vector < int > timeOffsetMillis;

	moca::PolyLine3f line;
    vector < ci::vec3 > widthInfo;       // x = direction * offset, y = distance, z = capPct
	vector < float > widths;
	void computeWidths();
	bool bAmILightning;
    

    
    void startDrawing(){
    	//timeToCheck = 0.2;
    	timeOffsetMillis.clear();
    	startTimeMillis = (getElapsedSeconds() * 1000);
    	durationMillis = 0;
    	playbackPos = 0;
    }

    void addedVertex(){

    	timeOffsetMillis.push_back(   (getElapsedSeconds() * 1000) - startTimeMillis );
    	durationMillis = (getElapsedSeconds() * 1000) - startTimeMillis;
    }

    //------------------------------------------------------------
    void finishDrawing(){
    	durationMillis = (getElapsedSeconds() * 1000) - startTimeMillis;
    	bAmDoneDrawing = true;
    	//ofLog() << "finished drawing " << line.size() << "," <<  timeOffsetMillis.size() << endl;

    }

    int lastFrameTime = 0;

    void update(){
        float delta_time = mTimer.getSeconds();
        mTimer.start();
    	if (timeOffsetMillis.size() > 0){
    	    	//timeToCheck = 0.98f * timeToCheck + 0.02 * timeOffsetMillis[timeOffsetMillis.size()-1] / 5.0;
    	    	//durationMillis = timeOffsetMillis[timeOffsetMillis.size()-1] - startTimeMillis + MAX(timeToCheck / 1000.0, 1.2)*1000;;

    	}


    	// if we don't have anything return;
    	if (line.size() == 0) return;

    	if (bAmDoneDrawing == true){
    		//ofLog() << "done drawing " << endl;
    		playbackPos += (int)(delta_time * 1000.0);
    		if (timeOffsetMillis.size() > 0){				// how could this possibly happen?  still check to be sure...
    			playbackPos %= (durationMillis + 1000);
    		}

    	} else {

    		//ofLog() << "not done drawing " << endl;
    		if (timeOffsetMillis.size() == 0) playbackPos = 0;
    		else playbackPos =  timeOffsetMillis[timeOffsetMillis.size()-1];

    	}

    	//ofLog() << " " << playbackPos << " ? " << endl;
    }

    float timeToCheck;
    int playbackPos;	// this should be the last point until I am done drawing
    bool bAmDoneDrawing;







};
