//
//  lineUtilities
//  DrawTests
//  Pretty much just a straight port of Lieberman's stuff.
//  https://github.com/ofZach/inkSpace
//  Created by Joseph Chow on 3/12/16.
//
//


#include "line.h"
#include "PLine.hpp"

// I take a "line" and put data into some vectors

void addPolyline ( 	lineWithInfo & line,
                  vector < vec3 > & currentPoints,
                  vector < vec3 > & prevPositions,
                  vector < vec3 > & nextPositions,
                  vector < vec3 > & lineInfo,
                  bool bAddExtraStartPoints,
                  bool bAddExtraEndPoints,
                  float addPct = 1.0);


void splitPolylineAt( moca::PolyLine3f & a,  moca::PolyLine3f& b,  moca::PolyLine3f& c, int pos);
void splitPolylineAt( moca::PolyLine3f& a,  moca::PolyLine3f& b,  moca::PolyLine3f& c,  moca::PolyLine3f& d, int posA, int posB);
void joinPolyline( moca::PolyLine3f& a,  moca::PolyLine3f& b,  moca::PolyLine3f& c);
void joinPolyline( moca::PolyLine3f& a,  moca::PolyLine3f& b,  moca::PolyLine3f& c, moca::PolyLine3f& d);
