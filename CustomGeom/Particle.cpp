//
//  ParticleGeom.cpp
//  InstancedFeedback
//
//  Created by Joseph Chow on 10/24/15.
//
//

#include "Particle.h"
using namespace std;
using namespace ci;
using namespace ci::geom;
// ParticleGeom
ParticleGeom::ParticleGeom()
{
    
}


ParticleGeom&	ParticleGeom::setScale( float scale)
{
    this->scale = scale;
    return *this;
}



uint8_t	ParticleGeom::getAttribDims( Attrib attr ) const
{
    switch( attr ) {
        case Attrib::POSITION: return 2;
        case Attrib::NORMAL: return 3;
        case Attrib::TEX_COORD_0: return 2;
        default:
            return 0;
    }
}

AttribSet ParticleGeom::getAvailableAttribs() const
{
    return { Attrib::POSITION, Attrib::NORMAL, Attrib::TEX_COORD_0 };
}

void ParticleGeom::loadInto( Target *target, const AttribSet &requestedAttribs ) const
{
    std::vector<vec3> positions;
    std::vector<vec2> texCoords;
    std::vector<vec3> normals;
    
    positions.push_back(vec3(0,0,0));
    positions.push_back(vec3(20,20,0));
    positions.push_back(vec3(-20,20,0));
    
    target->copyAttrib( Attrib::POSITION, 3, 0, (const float*)positions.data(), 3 );
    //target->copyAttrib( Attrib::NORMAL, 3, 0, (const float*)normals.data(), mNumVertices );
    //target->copyAttrib( Attrib::TEX_COORD_0, 2, 0, (const float*)texCoords.data(), mNumVertices );
}