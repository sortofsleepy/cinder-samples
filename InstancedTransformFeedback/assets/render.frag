#version 150
#extension all : warn

uniform sampler2D ParticleTex;

in float Transp;

out vec4 FragColor;

const vec3 pink = vec3(1.0, 0.07, 0.57);

void main() {
    FragColor = vec4(1.0,1.0,0.0,1.0);
}