#ifndef FxComposer_hpp
#define FxComposer_hpp

#include "post/FxPass.hpp"
#include <vector>


class FxComposer {

    std::vector<FxPass*> passes;
public:
    FxComposer();
    void addPass(FxPass * pass);
    
};

#endif 