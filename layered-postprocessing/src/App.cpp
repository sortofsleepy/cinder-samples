#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "cinder/app/RendererGl.h"
#include "cinder/Log.h"
#include "Sonics.hpp"

#include "ArcCam.hpp"
#include "core.hpp"
#include "objects/SoundSphere.hpp"
#include "post/FxPass.hpp"
#include "objects/Enviroment.hpp"

using namespace ci;
using namespace app;

class AppApp : public ci::app::App {
public:
    void setup() override;
    void update() override;
    void draw() override;
    ArcCamRef mCam;
    Enviroment env;
    
    FxPass layer1,layer2;
    
    
   
    ci::gl::VboMeshRef mMesh;
    ci::gl::BatchRef mBatch;
    ci::gl::GlslProgRef mShader,mShader2;
    gl::TextureRef tex;
    
    ci::gl::FboRef mFbo,mFbo2;
    SoundSphere sp;
    
    float sceneIndex = 0;
    float fxIndex = 1;
    float width = app::getWindowWidth();
    float height = app::getWindowHeight();
};


void AppApp::setup(){
    mCam = ArcCam::create();
    mCam->setZoom(100.0);
    
    env.setup();
    
    layer1.setup();
    layer2.setup().setShader("post/blur.glslf");
  
    sp.setup();
    sp.setShader("soundsphere/sphere.glslv","soundsphere/sphere.glslf");
    
                                  
                
}

void AppApp::update(){
    layer1.bind();
    gl::clear();
    mCam->useMatrices();
    env.draw();
    layer1.unbind();
    

}

void AppApp::draw(){
    gl::clear(Color(0,0,0));
    gl::enableAdditiveBlending();
    gl::setMatricesWindow(getWindowSize());
    gl::viewport(getWindowSize());
   
   
    layer1.draw();
    sp.draw(mCam);
}


CINDER_APP( AppApp, RendererGl,[] ( App::Settings *settings ) {
	settings->setWindowSize( 1280, 720 );
	settings->setMultiTouchEnabled( false );
} )