#version 150
uniform float theta;
uniform mat4 ciModelViewProjection;
in vec3 ciPosition;
in vec3 offset;

#include "../shaders/rotate.glslv"
void main(){
    
    vec3 pos = ( ciPosition + offset );
    
    // rotate
    pos = rotateY(pos,theta);
    pos = rotateZ(pos,theta);
    pos = rotateX(pos,theta);
    
    gl_Position = ciModelViewProjection * vec4(pos,1.);
  
}