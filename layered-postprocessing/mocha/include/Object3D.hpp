#ifndef Object3D_hpp
#define Object3D_hpp

#include "core.hpp"
#include "cinder/GeomIo.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Batch.h"

using namespace ci;
using namespace std;

// More of a descriptor class for 3D objects, provides several helpful methods for
// quickly building objects. Most if not all methods can be chained.

class Object3D {
public:
    ci::gl::BatchRef mBatch;
    ci::gl::VboMeshRef mMesh;
    ci::gl::GlslProgRef mShader;
    ci::gl::VboMesh::Layout layout;
    
    //! custom model matrix - can ignore if all animation occurs inside of GLSL
    ci::mat4 modelMatrix;
    
    Object3D(){}
    
    //! draws the object
    virtual void draw(){
        mBatch->draw();
    }
    
    gl::GlslProgRef getShader(){
        return mShader;
    }
    
    //! centers an item based on the width and height specified.
    //! Note that this only works with a perspective matrix and that
    //! you should put only use this in a push/pop matrices block.
    void centerItem(int width,int height){
        gl::translate(-width / 2,height / 2);
    }
    
    //! Initializes a Batch object. Should only call after setting up a mesh and shader
    Object3D& initialize(){
        mBatch = gl::Batch::create(mMesh,mShader);
        return *this;
    }
    
    //! rotates an object. Note - this doesn't return the rotation matrix itself, but
    //! simply changes the model matrix of the object.
    Object3D& rotate(float deg,ci::vec3 orient){
        modelMatrix = glm::rotate(modelMatrix,ci::toRadians(deg),orient);
        return *this;
    }
    
    //! Translates an object 
    Object3D& translate(ci::vec3 newCoords){
        modelMatrix = glm::translate(modelMatrix,newCoords);
        return *this;
    }
    
    //! Helper method to set a default shader to be the shader for the object
    Object3D& setStockShader(gl::ShaderDef stock){
        mShader = gl::getStockShader(stock);
        return *this;
    }
    
    //! loads a custom shader for your object
    virtual Object3D& loadShader(string vertex, string fragment="", string geom=""){
        mShader = mocha::loadShader(vertex,fragment,geom);
        return *this;
    }
};

#endif
