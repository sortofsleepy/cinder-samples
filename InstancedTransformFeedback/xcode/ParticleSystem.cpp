//
//  ParticleSystem.cpp
//  InstancedAnimationTest
//
//  Created by Joseph Chow on 10/29/15.
//
//

#include "ParticleSystem.h"
ParticleSystem::ParticleSystem(){}

void ParticleSystem::loadTexture()
{
    gl::Texture::Format mTextureFormat;
    mTextureFormat.magFilter( GL_LINEAR ).minFilter( GL_LINEAR ).mipmap().internalFormat( GL_RGBA );
    mSmokeTexture = gl::Texture::create( loadImage( loadAsset( "smoke_blur.png" ) ), mTextureFormat );
    
}
void ParticleSystem::setup()
{
  
    loadTexture();
    loadShaders();
    loadBuffers();
}

void ParticleSystem::loadBuffers(){
    
    app::console()<<" Using interleaved";
    float time = 0.0f;
    float rate = 0.001f;
    
    
    for(int i = 0; i < nParticles;++i){
        Particle p;
        p.position = Rand::randVec3() * randVec3();
       // p.color = ColorA( CM_HSV, lmap<float>( i, 0.0f, particles.size(), 0.0f, 0.66f ), 1.0f, 1.0f,1.0f );
        p.velocity = ci::randVec3() * mix( 0.0f, 1.5f, mRand.nextFloat() );
        p.startTime = time;
        particles.push_back(p);
        
        time += rate;
        
    }
    std::vector<vec3> positions( nParticles, vec3( 0.0f ) );
    // Reuse the positions vector that we've already made
    std::vector<vec3> normals = std::move( positions );
    
    for( auto normalIt = normals.begin(); normalIt != normals.end(); ++normalIt ) {
        // Creating a random velocity for each particle in a unit sphere
        *normalIt = ci::randVec3() * mix( 0.0f, 1.5f, mRand.nextFloat() );
    }
    
    
    // Create an initial velocity buffer, so that you can reset a particle's velocity after it's dead
    mPInitVelocity = ci::gl::Vbo::create( GL_ARRAY_BUFFER,	normals.size() * sizeof(vec3), normals.data(), GL_STATIC_DRAW );
    
    
    
    
    buffer.setData(particles, [=]()->void{
        gl::vertexAttribPointer( PositionIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, position) );
        gl::enableVertexAttribArray(PositionIndex);
        
        gl::vertexAttribPointer( VelocityIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, velocity) );
        gl::enableVertexAttribArray(VelocityIndex);
        
        gl::vertexAttribPointer( StartTimeIndex, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, startTime) );
        gl::enableVertexAttribArray(StartTimeIndex);
        
        mPInitVelocity->bind();
        ci::gl::vertexAttribPointer( InitialVelocityIndex, 3, GL_FLOAT, GL_FALSE, 0, 0 );
        ci::gl::enableVertexAttribArray( InitialVelocityIndex );
        
    });

    
  }
void ParticleSystem::loadShaders()
{
    try {
        // Create a vector of Transform Feedback "Varyings".
        // These strings tell OpenGL what to look for when capturing
        // Transform Feedback data. For instance, Position, Velocity,
        // and StartTime are variables in the updateSmoke.vert that we
        // write our calculations to.
        std::vector<std::string> transformFeedbackVaryings( 3 );
        transformFeedbackVaryings[PositionIndex] = "Position";
        transformFeedbackVaryings[VelocityIndex] = "Velocity";
        transformFeedbackVaryings[StartTimeIndex] = "StartTime";
        
        ci::gl::GlslProg::Format mUpdateParticleGlslFormat;
        // Notice that we don't offer a fragment shader. We don't need
        // one because we're not trying to write pixels while updating
        // the position, velocity, etc. data to the screen.
        mUpdateParticleGlslFormat.vertex( loadAsset( "updateSmoke.vert" ) )
        // This option will be either GL_SEPARATE_ATTRIBS or GL_INTERLEAVED_ATTRIBS,
        // depending on the structure of our data, below. We're using multiple
        // buffers. Therefore, we're using GL_SEPERATE_ATTRIBS
        .feedbackFormat( GL_INTERLEAVED_ATTRIBS )
        // Pass the feedbackVaryings to glsl
        .feedbackVaryings( transformFeedbackVaryings )
        .attribLocation( "VertexPosition",			PositionIndex )
        .attribLocation( "VertexVelocity",			VelocityIndex )
        .attribLocation( "VertexStartTime",			StartTimeIndex )
        .attribLocation( "VertexInitialVelocity",	InitialVelocityIndex )
        .attribLocation( "VertexColor", ColorIndex);
    
        
        mPUpdateGlsl = ci::gl::GlslProg::create( mUpdateParticleGlslFormat );
    }
    catch( const ci::gl::GlslProgCompileExc &ex ) {
        console() << "PARTICLE UPDATE GLSL ERROR: " << ex.what() << std::endl;
    }
    
    mPUpdateGlsl->uniform( "H", 1.0f / 60.0f );
    mPUpdateGlsl->uniform( "Accel", vec3( 0.0f ) );
    mPUpdateGlsl->uniform( "ParticleLifetime", 3.0f );
    
    
    buffer.setUpdateShader(mPUpdateGlsl);
    //buffer.setRenderShader(mPRenderGlsl);
}

void ParticleSystem::setBaseObject(ci::gl::VboMeshRef object){
   
    // create the VBO which will contain per-instance (rather than per-vertex) data
    system = buffer.getBuffer(0);
    
    //create layout for data.
    geom::BufferLayout instanceDataLayout;
    instanceDataLayout.append( geom::CUSTOM_0, 3, sizeof(Particle), offsetof(Particle, position ), 1 );
    
    //buffer Particle data onto your instanced mesh
    system->bufferSubData(0,sizeof(Particle) * particles.size(), particles.data());
    
    // now add it to the VboMesh we already made of the Teapot
    object->appendVbo( instanceDataLayout, system );
    
    //build out a rendering shader
    gl::GlslProg::Format fmt;
    fmt.vertex(app::loadAsset("render.vert"));
    fmt.fragment(app::loadAsset("render.frag"));
    gl::GlslProgRef render = gl::GlslProg::create(fmt);
    
    //build out the batch object with a reference to our instanced attribute
    mBatch = gl::Batch::create( object, render, {
        { geom::Attrib::CUSTOM_0, "VertexPosition" }
    } );
    
}

void ParticleSystem::update()
{
    buffer.update([=]()->void{
        gl::bindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, buffer.getDrawBuffer());
    });
}

void ParticleSystem::draw()
{
    // clear out the window with black
    gl::clear( Color( 0, 0, 0 ) );
    static float rotateRadians = 0.0f;
    rotateRadians += 0.01f;
    buffer.bind();
    gl::ScopedGlslProg		glslScope( mPRenderGlsl );
    gl::ScopedTextureBind	texScope( mSmokeTexture );
    gl::ScopedState			stateScope( GL_PROGRAM_POINT_SIZE, true );
    gl::ScopedBlend			blendScope( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    
    gl::pushMatrices();
    gl::multModelMatrix( rotate( rotateRadians, vec3( 0, 1, 0 ) ) );
    
    gl::setDefaultShaderVars();

    mBatch->drawInstanced(nParticles);
    gl::popMatrices();
    
}

