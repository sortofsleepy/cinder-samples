#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/CameraUi.h"
using namespace ci;
using namespace ci::app;
using namespace std;

class ReflectionTestsApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;
    
    ci::gl::VboMeshRef shape;
    ci::gl::GlslProgRef shader;
    ci::gl::BatchRef batch;
    
    ci::gl::TextureRef mTex;
    
    mat4 scale,mRotate;
    float rotateAngle = 0.0;
    CameraPersp mCam;
    CameraUi mCamUi;
};

void ReflectionTestsApp::setup()
{
    mCam.setPerspective(60.0, getWindowAspectRatio(), 1.0, 1000.0);
    mCam.lookAt(vec3(0,0,300),vec3(0));
    mCamUi.setCamera(&mCam);
    
    mTex = gl::Texture::create(loadImage(loadAsset("matcap.jpg")));
    
    gl::GlslProg::Format fmt;
    fmt.vertex(loadAsset("matcap.glsl"));
    fmt.fragment(loadAsset("shade.glsl"));
    
    auto geo = geom::Icosahedron() >>  geom::Subdivide() >> geom::Subdivide() >> geom::Subdivide();
    //auto geo = geom::Sphere().radius(30);
    shape = gl::VboMesh::create(geo);
    //shader = gl::getStockShader(gl::ShaderDef().color());
    shader = gl::GlslProg::create(fmt);
    batch = gl::Batch::create(shape, shader);
    
    scale = glm::scale(scale, vec3(70));
}

void ReflectionTestsApp::mouseDown( MouseEvent event )
{
}

void ReflectionTestsApp::update()
{

    mRotate *= rotate( toRadians( 0.3f ), normalize( vec3( 1, 1, 1 ) ) );
}

void ReflectionTestsApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
    gl::setMatrices(mCam);
    gl::enableDepth();
    
    //gl::enableAdditiveBlending();
    
    gl::ScopedGlslProg shd(shader);
    gl::ScopedTextureBind tex0(mTex,0);
    shader->uniform("tMatCap",0);
    gl::ScopedModelMatrix matrix;
    gl::multModelMatrix(scale);
    gl::multModelMatrix(mRotate);
    batch->draw();
}

CINDER_APP( ReflectionTestsApp, RendererGl )
