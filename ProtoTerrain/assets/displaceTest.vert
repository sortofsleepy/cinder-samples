#version 150
uniform float uTexOffset;
uniform mat4 ciModelViewProjection;
uniform float time;

in vec4 ciPosition;
in vec4 ciColor;
in vec2 ciTexCoord0;

out vec4 vertColor;
out vec2 vertTexCoord0;

#include "noise3d.glsl"

float surface3( vec3 coord ) {
    
    float n = 0.0;

    n += 1.0 * abs( snoise3( coord ) );
    n += 0.5 * abs( snoise3( coord * 2.0 ) );
    n += 0.25 * abs( snoise3( coord * 4.0 ) );
    n += 0.125 * abs( snoise3( coord * 8.0 ) );
    
    return n;
    
}

void main(){
    
    
    // retrieve texture coordinate and offset it to scroll the texture
    vec2 coord = ciTexCoord0 + vec2(0.0, uTexOffset);
    
    // offset the vertex based on the decibels
    vec4 vertex = ciPosition;
 
    float n = surface3(vec3(coord,1.0));
    vec3 calc = vec3(n,n,n);
    
    vertex.y += calc.y * calc.z * calc.x * 150.0;
    
    // pass (unchanged) texture coordinates, bumped vertex and vertex color
    vertTexCoord0 = ciTexCoord0;
    vertColor = ciColor;
    gl_Position = ciModelViewProjection * vertex;
}