#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/ObjLoader.h"
#include "cinder/ImageIo.h"
#include "cinder/Utilities.h"

using namespace ci;
using namespace ci::app;
using namespace std;

/**
 *  This is meant to demonstrate instanced drawing as well as how to upload data
 *  onto your VboMesh in a way with minimal headaches.
 *
 *  Primarily derrived from the InstancedTeapots example as well as the following two threads on the forumns
 *  https://forum.libcinder.org/topic/question-on-buffersubdata-offset#Search/CUSTOM_0
 *  https://forum.libcinder.org/topic/passing-a-mat4-to-shader-as-an-attribute-for-per-instance-transforms
 *  https://www.safaribooksonline.com/library/view/opengl-programming-guide/9780132748445/app09lev1sec2.html
 */


/**
 *  To start off, though you certainly don't have to, data is probably best kept in a
 *  container of some sort, in order to make things easier when uploading to your mesh.
 *
 *  Data is best kept in multiples of 4 according to the
 *  std140 layout rules(see safari link above), in this case, we have 6 floats + 2 floats in order to make
 *  a total of 8.
 */
typedef struct {
    ci::vec3 position;
    ci::vec3 color;
    
    ci::vec2 reserved;
}Particle;

class InstanceTestingApp : public App {
public:
    void setup();
    void resize();
    void update();
    void draw();
    
    //camera in order to draw 3D
    CameraPersp			mCam;
    
    // batch object for bringing in shader and mesh for drawing
    gl::BatchRef		mBatch;
    
    // shader for rendering
    gl::GlslProgRef		mGlsl;
    
    //Vbo to hold instanced data.
    gl::VboRef			mInstanceDataVbo;
    
    // a vector containing all of the Particles we want to draw
    std::vector<Particle> particles;
};

const int NUM_INSTANCES_X = 33;
const int NUM_INSTANCES_Y = 33;
const float DRAW_SCALE = 200;
const pair<float,float> CAMERA_Y_RANGE( 32, 80 );

void InstanceTestingApp::setup()
{
    mCam.lookAt( vec3( 0, CAMERA_Y_RANGE.first, 0 ), vec3( 0 ) );
    
    mGlsl = gl::GlslProg::create( loadAsset( "shader.vert" ), loadAsset( "shader.frag" ) );
    
    //create the mesh object we're drawing and instancing
    gl::VboMeshRef mesh = gl::VboMesh::create( geom::Teapot().subdivisions( 4 ) );
    
    //loop through and create particles.
    for( size_t potX = 0; potX < NUM_INSTANCES_X; ++potX ) {
        for( size_t potY = 0; potY < NUM_INSTANCES_Y; ++potY ) {
            float instanceX = potX / (float)NUM_INSTANCES_X - 0.5f;
            float instanceY = potY / (float)NUM_INSTANCES_Y - 0.5f;
            Particle p;
            p.position = vec3( instanceX * vec3( DRAW_SCALE, 0, 0 ) + instanceY * vec3( 0, 0, DRAW_SCALE ) ) ;
            Color c = Color( CM_HSV, lmap<float>( potX * potY, 0.0f, particles.size(), 0.0f, 0.66f ), 1.0f, 1.0f );
            
            p.color = vec3(c.r,c.g,c.b);
            particles.push_back(p);
        }
    }
    
    // create the VBO which will contain per-instance (rather than per-vertex) data
    mInstanceDataVbo = gl::Vbo::create( GL_ARRAY_BUFFER, particles.size() * sizeof(Particle),nullptr, GL_DYNAMIC_DRAW );
    
    //create layout for data.
    geom::BufferLayout instanceDataLayout;
    instanceDataLayout.append( geom::CUSTOM_0, 3, sizeof(Particle), offsetof(Particle, position ), 1 );
    instanceDataLayout.append( geom::CUSTOM_1, 3, sizeof(Particle), offsetof(Particle, color ), 1 );
    
    //buffer Particle data onto your instanced mesh
    mInstanceDataVbo->bufferSubData(0,sizeof(Particle) * particles.size(), particles.data());
    
    // now add it to the VboMesh we already made of the Teapot
    mesh->appendVbo( instanceDataLayout, mInstanceDataVbo );
    
    // and finally, build our batch, mapping our CUSTOM_0 attribute to the "vInstancePosition" GLSL vertex attribute
    mBatch = gl::Batch::create( mesh, mGlsl, {
        { geom::Attrib::CUSTOM_0, "vInstancePosition" },
        { geom::Attrib::CUSTOM_1  ,"vColor"}
    } );
    
    gl::enableDepthWrite();
    gl::enableDepthRead();
    
}

void InstanceTestingApp::resize()
{
    // now tell our Camera that the window aspect ratio has changed
    mCam.setPerspective( 60, getWindowAspectRatio(), 1, 1000 );
    
    // and in turn, let OpenGL know we have a new camera
    gl::setMatrices( mCam );
}

void InstanceTestingApp::update()
{
    //in the Instanced Teapots example, the camera annoyingly moves back and forth. Got rid of that
}

void InstanceTestingApp::draw()
{
    gl::clear( Color::black() );
    gl::setMatrices( mCam );
    
    //draw everything
    mBatch->drawInstanced( NUM_INSTANCES_X * NUM_INSTANCES_Y );
}

#if defined( CINDER_MSW ) && ! defined( CINDER_GL_ANGLE )
auto options = RendererGl::Options().version( 3, 3 ); // instancing functions are technically only in GL 3.3
#else
auto options = RendererGl::Options(); // implemented as extensions in Mac OS 10.7+
#endif
CINDER_APP( InstanceTestingApp, RendererGl( options ) )