#ifndef Enviroment_hpp
#define Enviroment_hpp
#include "cinder/gl/Shader.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"
#include "core.hpp"
#include "Object3D.hpp"
#include "cinder/Vector.h"

class Enviroment {
    
    ci::gl::VboMeshRef mEnvMesh;
    ci::gl::GlslProgRef mEnvShader;
    ci::gl::BatchRef mEnvBatch;
    float domeRadius;
    ci::gl::TextureRef mTex;    
    ci::vec3 getCenter(ci::vec3 p0, ci::vec3 p1);
    ci::vec3 getPosition(int i, int j);
    ci::vec3 getNormal(ci::vec3 p0,ci::vec3 p1, ci::vec3 p2);
    
    
    // texture for the enviroment
    ci::gl::TextureRef mEnvTex;
    ci::gl::TextureRef mGradientTexture;
    
    // pbr stuff
    ci::gl::TextureCubeMapRef mIrradianceMap,mRadianceMap;
    
    float gamma;
    float exposure;
    float roughness;
    float specular;
    float metallic;
    ci::vec3 baseColor;
    
    
public:
    Enviroment();
    void setupPbr();
    void setup();
    void draw();
};

#endif