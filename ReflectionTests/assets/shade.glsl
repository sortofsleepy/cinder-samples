#version 150
in vec2	TexCoord;
in vec4	Color;
in vec2	Normal;
uniform sampler2D tMatCap;
out vec4 glFragColor;
void main(){
    
    vec3 base = texture(tMatCap,Normal).rgb;
    glFragColor = vec4(base,1.0);
}