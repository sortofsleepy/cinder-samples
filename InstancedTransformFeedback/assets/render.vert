#version 150

uniform mat4	ciModelViewProjection;
uniform mat3	ciNormalMatrix;

in vec4		ciPosition;
in vec2		ciTexCoord0;
in vec3		ciNormal;
in vec4		ciColor;
in vec3		VertexPosition; // per-instance position variable


uniform float time;


out highp vec2	TexCoord;
out lowp vec4	Color;
out highp vec3	Normal;

void main( void )
{
  
    vec4 pos = ciModelViewProjection * ( 4.0 * ciPosition + vec4( VertexPosition, 0 ) );
    
    pos.x += time;
    
    gl_Position	= pos;
    Color 		= ciColor;
    TexCoord	= ciTexCoord0;
    Normal		= ciNormalMatrix * ciNormal;
}
