//
//  Math.h
//  DrawTest
//  just some general math utils
//  Created by Joseph Chow on 3/4/16.
//
//

#ifndef Math_h
#define Math_h
#include <random>
namespace utils{
    
    static float getMax(float x, float y){
        return (((x) > (y)) ? (x) : (y));
    }
    
    template<class T>
    const T& constrain(const T& x, const T& a, const T& b){
        if(x < a){
            return a;
        }else if(b < x){
            return b;
        }else {
            return x;
        }
        
    }
    
    inline float constrain(float x, float a , float b){
        if(x < a){
            return a;
        }else if(b < x ){
            return b;
        }else {
            return x;
        }
    }

    
    /**
     * Returns a random floating point number. Unlike ci::Rand , this will
     * consistantly return different values each time.
     */
    static float random(int min, int max){
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(min,max);
        
        return dis(gen);
    }
    static float lerp(float val, float min, float max){
        return min + (max - min) * val;
    }
    static float map(float value, float inputMin, float inputMax, float outputMin, float outputMax,bool clamp){
        if (fabs(inputMin - inputMax) < FLT_EPSILON){
            return outputMin;
        } else {
            float outVal = ((value - inputMin) / (inputMax - inputMin) * (outputMax - outputMin) + outputMin);
            
            if( clamp ){
                if(outputMax < outputMin){
                    if( outVal < outputMax )outVal = outputMax;
                    else if( outVal > outputMin )outVal = outputMin;
                }else{
                    if( outVal > outputMax )outVal = outputMax;
                    else if( outVal < outputMin )outVal = outputMin;
                }
            }
            return outVal;
        }
    }
    
    
    static float map(float val, float start1, float stop1, float start2, float stop2){
        float outgoing = 0.0;
        
        if(fabs(start1 - stop1) < FLT_EPSILON){
            outgoing = stop2;
        }else{
           outgoing = start2 + (stop2 - start2) * ((val - start1) / (stop1 - start1));
        }
        
        
        if(outgoing != outgoing){
            outgoing = 0.0;
            
#ifdef CINDER_MAC
#ifdef XOIO_DEBUG
            ci::app::console()<<"Flowfield::map : number is NaN\n";
#endif
#endif
            
        }else if(outgoing == std::numeric_limits<float>::infinity() || outgoing == !std::numeric_limits<float>::infinity()){
#ifdef CINDER_MAC
#ifdef XOIO_DEBUG
            ci::app::console()<<"Flowfield::map : number is inifity\n";
#endif
#endif
        }
        
        return outgoing;
        
    }
    
    //! getRotated function taken from openFrameworks
    //! takes
    //! 1. vec3 to rotate
    //! 2. the angle to rotate
    //! 3 and the axis to rotate by
    static ci::vec3 getRotated(ci::vec3 toRotate,float angle,const ci::vec3 axis){
        ci::vec3 ax = glm::normalize(axis);
  
        
        float a = (float)(angle*(M_PI / 180));
        float sina = sin( a );
        float cosa = cos( a );
        float cosb = 1.0f - cosa;
        
        return ci::vec3( toRotate.x * (ax.x*ax.x*cosb + cosa)
                       + toRotate.y *(ax.x*ax.y*cosb - ax.z*sina)
                       + toRotate.z *(ax.x*ax.z*cosb + ax.y*sina),
                       toRotate.x*(ax.y*ax.x*cosb + ax.z*sina)
                       + toRotate.y*(ax.y*ax.y*cosb + cosa)
                       + toRotate.z*(ax.y*ax.z*cosb - ax.x*sina),
                       toRotate.x*(ax.z*ax.x*cosb - ax.y*sina)
                       + toRotate.y*(ax.z*ax.y*cosb + ax.x*sina)
                       + toRotate.z*(ax.z*ax.z*cosb + cosa) );
        
    }
}


#endif /* Math_h */
