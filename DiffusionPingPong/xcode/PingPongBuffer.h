//
//  Buffer.h
//  Diffusion
//
//  Created by Joseph Chow on 8/31/15.
//
//

#ifndef __Diffusion__Buffer__
#define __Diffusion__Buffer__

#include "cinder/gl/Fbo.h"

#define SIZE 512

class PingPongBuffer {
    
    int mCurrentFBO, mOtherFBO;
    cinder::gl::FboRef buffer;
public:
    PingPongBuffer();
    
    //! runs the shader across both buffers
    //! While the correct buffers will get bound, you
    //! still need to pass in a callback function to specify the processing
    void update(std::function<void()> fn);
    
    //! resets the buffers
    void resetBuffers();
    cinder::Area getBounds();
    //! gets the buffer we're currently writing to
    ci::gl::TextureRef getCurrentBuffer();
    
    //! gets the buffer whose texture is currently being used as data
    ci::gl::TextureRef getOtherBuffer();
    
    //! returns the current output after running the shader;
    ci::gl::TextureRef getOutput();
};

#endif /* defined(__Diffusion__Buffer__) */
