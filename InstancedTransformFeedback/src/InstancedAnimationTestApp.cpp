#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Camera.h"
#include "cinder/gl/Shader.h"
#include "cinder/Rand.h"
#include "ParticleSystem.h"

/**
 *  This is a example of how to use transform feedback to animate a instanced set of gl::VboMesh's
 *  A lot of the code follows very closely with existing Cinder samples like the "TransformFeedbackSmokeParticles"
 *  as well as the "InstancedTeapots" example though the transform feedback aspects have be abstracted out
 *  into a seperate class called "FeedbackBuffer" in order to try and simplify things a bit.
 *
 *  The final result should be a amorphous blob of yellow teapots :)
 *
 */

using namespace ci;
using namespace ci::app;
using namespace std;

class InstancedAnimationTestApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;
    
    // The ParticleSystem class will represent our instanced meshes
    ParticleSystem system;
    
    // need a camera cause we're doing 3D
    CameraPersp mCam;

};

void InstancedAnimationTestApp::setup()
{

    //set perspective and lookAt position
    mCam.setPerspective( 60.0f, getWindowAspectRatio(), .01f, 1000.0f );
    mCam.lookAt( vec3( 0, 0, 10 ), vec3( 0, 0, 0 ) );
    
    //create the base object thats going to be instanced. In this case, we keep it simple
    // and just use one of the predefined geometries. However, you could just as easily subclass
    // geom::Source and come up with your own geometries as well
    gl::VboMeshRef mesh = gl::VboMesh::create(geom::Teapot().subdivisions(3));
    
    //setup the particle system
    system.setup();
    
    //set the base object we're using
    system.setBaseObject(mesh);
    
}

void InstancedAnimationTestApp::mouseDown( MouseEvent event )
{
}

void InstancedAnimationTestApp::update()
{
    system.update();
}

void InstancedAnimationTestApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
    gl::setMatrices(mCam);
    system.draw();
    
}

CINDER_APP( InstancedAnimationTestApp, RendererGl )
