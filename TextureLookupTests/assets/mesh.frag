#version 150

uniform sampler2D fieldLookup;
uniform vec2 desiredPosition;
in vec2 TexCoord;
out vec4 oColor;

void main()
{
    // When setting up the texture, we set the 0 index to be yellow, so we look that up.
    // The second parameter is a vec2 position which, normally sent the texture coordinates of
    // the geometry you're trying to texture. In this case though, we're not looking to use
    // "fieldLookup" to apply a image to the sphere but instead, use it as a storage device of sorts.
    // Therefore, we can pass vec2(0,0) as the second parameter in order to get the sphere to become yellow,
    // because that was the value we set during our texture initialization.
    vec4 data = texture(fieldLookup,desiredPosition.xy);
    
    oColor = vec4(data.xyz,1.0);
    //oColor = vec4(1.0,1.0,0.0,1.0);
    
}