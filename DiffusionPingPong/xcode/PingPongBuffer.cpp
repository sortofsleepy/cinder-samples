//
//  Buffer.cpp
//  Diffusion
//
//  Created by Joseph Chow on 8/31/15.
//
//

#include "PingPongBuffer.h"
using namespace ci;
using namespace std;

PingPongBuffer::PingPongBuffer(){
    mCurrentFBO = 0;
    mOtherFBO = 1;
    
    gl::Texture::Format texFmt;
   
    
    gl::Fbo::Format fmt;
    fmt.colorTexture();
    fmt.disableDepth();
    fmt.setColorTextureFormat(gl::Texture2d::Format().wrap( GL_CLAMP_TO_EDGE ).internalFormat(GL_RGBA32F_ARB));
    fmt.attachment(GL_COLOR_ATTACHMENT0 + 0, gl::Texture2d::create( SIZE,SIZE,texFmt));
    fmt.attachment(GL_COLOR_ATTACHMENT0 + 1, gl::Texture2d::create(SIZE, SIZE,texFmt));
    
    buffer = gl::Fbo::create(SIZE, SIZE,fmt);
}

void PingPongBuffer::update(std::function<void ()> fn){
    
    //You don't really need iterations but it helps
    //speed things up a bit
    const int ITERATIONS = 25;
    
    // normally setMatricesWindow flips the projection vertically so that the upper left corner is 0,0
    // but we don't want to do that when we are rendering the FBOs onto each other, so the last param is false
    gl::setMatricesWindow( buffer->getSize() );
    gl::viewport( buffer->getSize() );
    
    for( int i = 0; i < ITERATIONS; i++ ) {
        mCurrentFBO = ( mCurrentFBO + 1 ) % 2;
        mOtherFBO   = ( mCurrentFBO + 1 ) % 2;
        
        gl::ScopedFramebuffer fboBind( buffer );
        
        {
            gl::drawBuffer(GL_COLOR_ATTACHMENT0 + mCurrentFBO);
            
            gl::ScopedTextureBind texture( getOtherBuffer(), 0 );
            fn();
            
           
        }
        
        
    }
    
}


Area PingPongBuffer::getBounds(){
    return buffer->getBounds();
}


void PingPongBuffer::resetBuffers(){
    gl::setMatricesWindow( buffer->getSize() );
    gl::viewport( buffer->getSize() );
    
    gl::ScopedFramebuffer fboBind( buffer );{
      
        for(int i = 0; i < 2; ++i){
            gl::drawBuffer(GL_COLOR_ATTACHMENT0 + i);
            gl::drawSolidRect( buffer->getBounds() );
            
        }
        
    }

}

gl::TextureRef PingPongBuffer::getCurrentBuffer(){
    
    return buffer->getTexture2d(GL_COLOR_ATTACHMENT0 + mCurrentFBO);
    //return tex[mCurrentFBO];
}

gl::TextureRef PingPongBuffer::getOtherBuffer(){
    return buffer->getTexture2d(GL_COLOR_ATTACHMENT0 + mOtherFBO);
}

gl::TextureRef PingPongBuffer::getOutput(){
    return buffer->getTexture2d(GL_COLOR_ATTACHMENT0 + mCurrentFBO);
}