### VBOInit ###

This one is pretty basic but I needed a good reminder of how to construct a vbo manually in Cinder. 
Turns out it's more or less the same basic steps to craft a vbo in regular opengl / webgl
Essentially the steps are (and some of these could be done out of order)


* build shader
* build data describing a particular attribute
* initialize vbo with said data
* intialize vao
* get reference to attribute data
* bind vao and vbo and do the usual `enableVertexAttribArray` + `vertexAttribPointer` calls
* when drawing, bind shader then vao and then you can call `drawArrays`
