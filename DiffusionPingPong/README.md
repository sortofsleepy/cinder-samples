__DiffusionPingPong__

The primary purpose of this example is to demonstrate one way to do GPU ping-ponging. It utilizes the 
sample code from the [RDiffusion](https://github.com/cinder/Cinder/tree/master/samples/RDiffusion) sample in the Cinder distribution. The ping-ponging has been extracted into 
it's own class and has a modified update loop where you can customize it a bit using C++11 lambda functions.

The end result should be a series of triangles tweening into view and the reaction diffusion is being applied as a texture onto those triangles.