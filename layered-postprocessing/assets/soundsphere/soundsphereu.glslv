#version 150 core
in vec3 VertexPosition;
in vec3 VertexVelocity;
in float VertexPhi;
in float VertexTheta;
in float VertexPhiSpeed;
in float VertexThetaSpeed;
out vec3 Position; // To Transform Feedback
out vec3 Velocity; // To Transform Feedback
out float Phi;
out float Theta;
out float PhiSpeed;
out float ThetaSpeed;
void main() {
    // Update position & velocity for next frame
    Position = VertexPosition;
    Velocity = VertexVelocity;
    Phi = VertexPhi;
    Theta = VertexTheta;
    PhiSpeed = VertexPhiSpeed;
    ThetaSpeed = VertexThetaSpeed;
    
    // increment phi and theta
    Phi += PhiSpeed * 0.05;
    Theta += ThetaSpeed * 0.05;
    
    float r = 80.0;
    
    float x = cos(Theta) * sin(Phi) * r;
    float y = sin(Theta) * sin(Phi) * r;
    float z = cos(Phi) * r;
    Position = vec3(x,y,z);
    
    
}
/*
 
    if( Time >= StartTime ) {
        float age = Time - StartTime;
        if( age > ParticleLifetime ) {
            // The particle is past it's lifetime, recycle.
            Position = VertexInitialPosition;
            Velocity = VertexInitialVelocity;
            StartTime = Time;
            
        }else {
            // The particle is alive, update.
            Position += Velocity * H;
            Velocity += Accel * H;
          
	}
	
    }
    
    */