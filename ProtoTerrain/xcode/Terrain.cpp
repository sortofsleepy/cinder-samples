//
//  Terrain.cpp
//  ProtoTerrain
//
//  Created by Joseph Chow on 2/19/16.
//
//

#include "Terrain.hpp"
using namespace ci;
using namespace ci::app;
using namespace std;


Terrain::Terrain(){
    
    //build shader
    gl::GlslProg::Format fmt;
    fmt.vertex(loadAsset("displaceTest.vert"));
    fmt.fragment(loadAsset("displaceTest.frag"));
    shader = gl::GlslProg::create(fmt);
    
    mOffset = 0;
    buildMesh();
}

void Terrain::updateNoise(){
    
    // increment texture offset
    mOffset = ( mOffset + 1 );
    
}

void Terrain::draw(){
    gl::ScopedGlslProg shd(shader);{
        shader->uniform("time", (float)getElapsedSeconds());
        shader->uniform("uTexOffset",mOffset / float( kHistory ) );
        mBatch->draw();
    }
}



void Terrain::buildMesh(){
    // create static mesh (all animation is done in the vertex shader)
    std::vector<vec3>		positions;
    std::vector<Colorf>		colors;
    std::vector<vec2>		coords;
    std::vector<uint32_t>	indices;
    
    for( size_t h = 0; h < kHeight; ++h ) {
        for( size_t w = 0; w < kWidth; ++w ) {
            // add polygon indices
            if( h < kHeight - 1 && w < kWidth - 1 ) {
                size_t offset = positions.size();
                
                indices.emplace_back( offset );
                indices.emplace_back( offset + kWidth );
                indices.emplace_back( offset + kWidth + 1 );
                indices.emplace_back( offset );
                indices.emplace_back( offset + kWidth + 1 );
                indices.emplace_back( offset + 1 );
            }
            
            // add vertex
            positions.emplace_back( vec3( float( w ), 0, float( h ) ) );
            
            // add texture coordinates
            // note: we only want to draw the lower part of the frequency bands,
            //  so we scale the coordinates a bit
            const float part = 0.5f;
            float s = w / float( kWidth );
            float t = h / float( kHeight);
            coords.emplace_back( vec2( part * s , t ) );
            
            // add vertex colors
            colors.emplace_back( Color( CM_HSV, s, 0.5f, 0.75f ) );
        }
    }
    
    gl::VboMesh::Layout layout;
    layout.usage( GL_STATIC_DRAW );
    layout.attrib( geom::Attrib::POSITION, 3 );
    layout.attrib( geom::Attrib::COLOR, 3 );
    layout.attrib( geom::Attrib::TEX_COORD_0, 2 );
    
    mesh = gl::VboMesh::create( positions.size(), GL_TRIANGLES, { layout }, indices.size(), GL_UNSIGNED_INT );
    mesh->bufferAttrib( geom::POSITION, positions.size() * sizeof( vec3 ), positions.data() );
    mesh->bufferAttrib( geom::COLOR, colors.size() * sizeof( vec3 ), colors.data() );
    mesh->bufferAttrib( geom::TEX_COORD_0, coords.size() * sizeof( vec2 ), coords.data() );
    mesh->bufferIndices( indices.size() * sizeof( uint32_t ), indices.data() );
    
    // create a batch for better performance
    mBatch = gl::Batch::create(mesh, shader );
}