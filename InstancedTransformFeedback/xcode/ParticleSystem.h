//
//  ParticleSystem.h
//  InstancedAnimationTest
//
//  Created by Joseph Chow on 10/29/15.
//
//

#ifndef InstancedAnimationTest_ParticleSystem_h
#define InstancedAnimationTest_ParticleSystem_h

#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "FeedbackBuffer.h"
#include "cinder/Rand.h"

using namespace ci;
using namespace ci::app;
using namespace std;

typedef struct {
    ci::vec3 position;
    ci::vec3 velocity;
    float startTime;

}Particle;

// This example is based on samples from the 10th chapter of the book, OpenGL 4.0
// Shading Language Cookbook. For more in-depth discussion of what is going on
// here please refer to that reference.

const int nParticles			= 7089;
const int PositionIndex			= 0;
const int VelocityIndex			= 1;
const int StartTimeIndex		= 2;
const int InitialVelocityIndex	= 3;
const int ColorIndex            = 4;

class ParticleSystem {
public:
    ParticleSystem();
    void setup();
    void update();
    void draw();
    

    ci::gl::VboRef system;
    ci::gl::BatchRef mBatch;
    
    void setBaseObject(ci::gl::VboMeshRef object);
    
    gl::GlslProgRef	mPUpdateGlsl, mPRenderGlsl;
    
    void loadBuffers();
    void loadShaders();
    void loadTexture();
    gl::TextureRef mSmokeTexture;
    gl::VboRef  mPInitVelocity;
    Rand							mRand;
    CameraPersp						mCam;
    TriMeshRef						mTrimesh;
    uint32_t						mDrawBuff;
    
    std::vector<Particle> particles;
    FeedbackBuffer buffer;
};



#endif
