#!/bin/bash

# same as run.sh but completely clears the build folder.
cd ../

# clear build folder and remake
rm -rf build
mkdir build

# go into build folder
cd build

# run cmake
cmake ..

# run make.
make

# start the generated app.
#./<path to repo folder>/cinder-cmake/build/Debug/App/App.app/Contents/MacOS/App