#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
#include "cinder/Camera.h"
#include "cinder/CameraUi.h"
#include "cinder/ImageIo.h"
#include "LookupTexture.h"
#include "MathUtils.h"
#include "cinder/Rand.h"
using namespace ci;
using namespace ci::app;
using namespace std;

class TextureLookupTestsApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;
    
    CameraPersp			mCamera;
    CameraUi			mCameraUi;
    gl::FboRef			mFbo;
    Surface32f pixels;
    gl::TextureRef test;
    LookupTexture lookup;
    ci::gl::TextureRef resultTexture;
     float resolution = 7;
    ci::gl::GlslProgRef renderShader;
};

void TextureLookupTestsApp::setup()
{
 
    //lookup = LookupTexture(resolution);
    float cols = app::getWindowWidth();
    float rows = app::getWindowHeight();

    //setup texture for lookup table
    pixels = Surface32f(cols,rows,true);
    
    for(int i = 0; i < cols; ++i){
        for(int j = 0; j < rows; ++j){
            pixels.setPixel(vec2(i,j), Color(0,0,0));
        }
    }
    
    //set positions in the Surface of values we want to store.
    pixels.setPixel(vec2(0,0), Color(1,1,0));
    pixels.setPixel(vec2(1,0), Color(1,0,1));
    pixels.setPixel(vec2(2,0), Color(0,0,1));
    
    //create teh texture
    test = gl::Texture::create(pixels);
    
    //setup the rendering shader
    gl::GlslProg::Format fmt;
    fmt.vertex(app::loadAsset("passthru.glsl"));
    fmt.fragment(app::loadAsset("mesh.frag"));
    renderShader = gl::GlslProg::create(fmt);
    
    //set our lookup "array"
    lookup.setup(pixels);
    
    //pick a position to lookup value for. Follows standard array rules with 0 being the first index
    lookup.lookupPosition(vec2(1,0));
    
}

void TextureLookupTestsApp::mouseDown( MouseEvent event )
{
}

void TextureLookupTestsApp::update()
{
    
}

void TextureLookupTestsApp::draw()
{
	gl::clear( Color( 1, 0, 0 ) );

    /**
     *  For the purposes of demonstration, we just draw the texture. 
     *  At this point, the entirety of the texture should be the value you looked up, so in this case,
     *  the screen should be magenta.
     */
    gl::ScopedTextureBind tex(lookup.getBuffer()->getColorTexture(),0);
    gl::ScopedGlslProg shd(renderShader);
    gl::drawSolidRect(Rectf(0,0,app::getWindowWidth(),app::getWindowHeight()));
   
}

CINDER_APP( TextureLookupTestsApp, RendererGl )
