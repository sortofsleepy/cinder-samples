#ifndef FxPass_hpp
#define FxPass_hpp

#include "cinder/Rect.h"
#include "cinder/Color.h"
#include "cinder/gl/Fbo.h"
#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/GlslProg.h"
#include "core.hpp"
#include <string>

using namespace ci;
using namespace std;

class FxPass {
protected:
    
    //! FBO used for post processing work.
    ci::gl::FboRef mFbo;
    
    int width = ci::app::getWindowWidth();
    int height = ci::app::getWindowHeight();
    
    int sceneIndex = 0;
    int fxIndex = 1;
    
    //! This is a texture reference used when slotting the pass in as part of a
    //! larger post-processing chain of effects.
    ci::gl::TextureRef mInput;
    
    //! post processing shader. By default, a pass-through texture shader.
    ci::gl::GlslProgRef mFxShader = gl::getStockShader(gl::ShaderDef().texture());
    
    std::string passthru = STRINGIFY(
        uniform mat4 ciModelViewProjection;
        in vec4 ciPosition;
        in vec2 ciTexCoord0;                             
        out vec2 uv;
        out vec2 vUv;                             
        const vec2 scale = vec2(0.5,0.5);
                                     
        void main(){
            uv = ciPosition.xy * scale + scale;
            vUv = ciTexCoord0;
            gl_Position = ciModelViewProjection * ciPosition;
        }
     );
public:
    FxPass(){}
    
    virtual FxPass& setup(){
        gl::Texture::Format fmt;
        fmt.wrap(GL_CLAMP_TO_EDGE);
        fmt.setMagFilter(GL_NEAREST);
        fmt.setMinFilter(GL_NEAREST);
        fmt.setInternalFormat(GL_RGBA);
        
        gl::Fbo::Format ffmt;
        ffmt.attachment(GL_COLOR_ATTACHMENT0 + sceneIndex, gl::Texture2d::create(width,height,fmt));
        ffmt.attachment(GL_COLOR_ATTACHMENT0 + fxIndex, gl::Texture2d::create(width,height,fmt));
        
        mFbo = gl::Fbo::create(width,height,ffmt);
        
        gl::ScopedFramebuffer fb(mFbo);{
            gl::clear();
        }
        return *this;
    }
    
    void setShader(string fragment, string vertex="", string geo=""){
        if(vertex == ""){
            vertex = passthru;
        }
        mFxShader = mocha::loadShader(vertex,fragment,geo);
    }
    
    virtual void processFx(){
        bind();
        
        selectFxLayer();
        gl::clear();
        
        // set window matrices
        gl::setMatricesWindow(mFbo->getSize());
        gl::viewport(mFbo->getSize());
         
        // bind fx shader
        mFxShader->bind();
       
        gl::ScopedTextureBind tex0(mFbo->getTexture2d(GL_COLOR_ATTACHMENT0),0);
        mFxShader->uniform("mInput",0);
        mFxShader->uniform("resolution",vec2(mFbo->getWidth(),mFbo->getHeight()));
                     
        gl::drawSolidRect(Rectf(0,0,width,height));
        unbind();
    }
    
    //! Processes the input texture using the Fx shader
    virtual void processInput(){
        bind();
        selectFxLayer();
        gl::clear();
        // set window matrices
        gl::setMatricesWindow(mFbo->getSize());
        gl::viewport(mFbo->getSize());
        // bind fx shader
        mFxShader->bind();
        gl::ScopedTextureBind tex0(mInput,0);
        gl::drawSolidRect(Rectf(0,0,width,height));
        unbind();
    }
    
    //! Binds the fbo for drawing onto the scene layer
    virtual void bind(){
        mFbo->bindFramebuffer();
        selectSceneLayer();
    }
    
    //! Unbinds fbo
    virtual void unbind(){
        mFbo->unbindFramebuffer();
    }
    
    //! Selects the Fx texture for drawing onto.
    //! Needs bind() called prior to calling this function.
    virtual void selectFxLayer(){
        gl::drawBuffer(GL_COLOR_ATTACHMENT0 + fxIndex);
    }
    //! Selects the scene texture for drawing onto.
    //! Needs bind() called prior to calling this function.    
    virtual void selectSceneLayer(){
        gl::drawBuffer(GL_COLOR_ATTACHMENT0 + sceneIndex);
    }
    
    //! Draws the final texture of the fbo. By default draws the scene layer,
    //! pass in 1 to draw the composite fx layer
    virtual void draw(int layer=0){
        if(layer != 0 && layer != 1){
            CI_LOG_E("Invalid layer specified in draw command");
            return;
        }
        
        gl::draw(mFbo->getTexture2d(GL_COLOR_ATTACHMENT0 + layer),Rectf(0,0,width,height));
        
    }
    
};

#endif