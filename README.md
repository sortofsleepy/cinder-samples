### Cinder-Samples ###

Just a series of projects demonstrating possible ways to approach OpenGL techniques in regards to the [Cinder](http://libcinder.org) framework that might not be already be covered by the current set of samples Cinder already provides. It's primarily here so that I don't forget how to do anything, but also made public in the event this happens to help someone else. 

Remember, the techniques demonstrated here are only ONE way to approach things. On top of that, it's also based on my understanding of the technique, which may or may not be incorrect. While I'd be flattered, it'd be best to not take these samples as gospel :)

### How do I get set up? ###
* All of the samples are for OSX (subsequently XCode). Currently the project files are setup to work from your documents folder. Create a folder called `app` and place the samples in there. Your Cinder distribution should have the folder name of `Cinder`

* It's of course, totally possible to change this in the project settings

### Contribution guidelines ###

* If theres a better way of approaching some of these techniques, I totally welcome pull requests.