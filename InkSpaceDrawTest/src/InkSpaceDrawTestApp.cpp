#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "line.h"
#include "lineUtilities.h"
#include "cinder/Camera.h"
using namespace ci;
using namespace ci::app;
using namespace std;

class InkSpaceDrawTestApp : public App {
public:
    void setup() override;
    void setupCamera();
    void mouseDown( MouseEvent event ) override;
    void mouseUp( MouseEvent event ) override;
    void mouseDrag( MouseEvent event ) override;
    void update() override;
    void draw() override;
    void setLocalMatrix();
    
    void loadShaders();
    void loadBuffer();
    void loadFbo();
    
    ci::CameraPersp mCam,drawCam;
    
    ci::gl::VboMeshRef mesh;
    ci::gl::GlslProgRef shader;
    ci::gl::BatchRef mBatch;
    
    ci::gl::FboRef canvas;
    
    lineWithInfo line;
    vector<lineWithInfo> lines;
    ci::mat4 mvMatrix;
    ci::mat4 pMatrix;
    gl::VaoRef vao;
    ci::vec2 viewportPos;
    vector<vec3> tempPoints;
    
    ci::mat4 localMatrix;
    ci::vec3 position,scale;
    ci::quat orientation;
    
    gl::TextureRef tex;
};

void InkSpaceDrawTestApp::setLocalMatrix(){
    /*
     Steps to obtain the correct modelview matrix for drawing
     1. set matrix to identity
     2. translate matrix by half of the width and height (z is the dist value from the camera setup)
     or 415.692 for a 640x480 window
     3. invert matrix;
     */
    
    localMatrix = glm::translate(vec3(getWindowWidth() / 2, getWindowHeight() / 2, 415.92));
    localMatrix = glm::inverse(localMatrix);
}

/**
 Need 2 cameras, one to render the mesh and one
 to use as the matrix for the drawing. Labeled mCam and drawCam
 accordingly.
 */
void InkSpaceDrawTestApp::setupCamera(){
    
    //setup perspective matrix on drawCam like the oF ofCamera::setupPerspective function
    float fov = 60.0;
    float eyeX = getWindowWidth() / 2;
    float eyeY = getWindowHeight() / 2;
    //float eyeX = ( 1080*0.5 ) / 2;
    //float eyeY = ( 1920*0.5 ) / 2;
    float halfFov = M_PI * fov / 360;
    float theTan = tanf(halfFov);
    float dist = eyeY / theTan;
    float nearDist = dist / 10.0f;
    float farDist = dist * 10.0f;
    
    drawCam.setFov(fov);
    drawCam.setNearClip(nearDist);
    drawCam.setFarClip(farDist);
    drawCam.lookAt(vec3(eyeX,eyeY,0));
    drawCam.setAspectRatio(getWindowWidth() / getWindowHeight());
    
    
    //setup the cam to render the mesh
    mCam.setPerspective(75, getWindowAspectRatio(),1,100);
    mCam.lookAt(vec3(0));
    
}

void InkSpaceDrawTestApp::loadShaders(){
    //load the shader
    gl::GlslProg::Format fmt;
    fmt.vertex(loadAsset("lineShader.vert"));
    fmt.fragment(loadAsset("lineShader.frag"));
    shader = gl::GlslProg::create(fmt);
    
}

void InkSpaceDrawTestApp::loadBuffer(){
    
    //create VboMesh
    gl::VboMesh::Layout layout;
    layout.usage(GL_DYNAMIC_DRAW);
    layout.attrib(geom::POSITION, 3);
    layout.attrib(geom::CUSTOM_0, 3);
    layout.attrib(geom::CUSTOM_1, 3);
    layout.attrib(geom::CUSTOM_2, 3);
    
    //initialize. 9999 for number of vertices to accomodate any sized line on refresh of data.
    mesh = gl::VboMesh::create(9999,GL_TRIANGLE_STRIP,{layout});
    
}

//build fbo
void InkSpaceDrawTestApp::loadFbo(){
    gl::Texture::Format tfmt;
    tfmt.loadTopDown(true);
    
    gl::Fbo::Format fmt;
    fmt.setColorTextureFormat(tfmt);
    canvas = gl::Fbo::create(getWindowWidth(), getWindowHeight(),fmt);
    canvas->bindFramebuffer();
    gl::clear();
    canvas->unbindFramebuffer();
}

void InkSpaceDrawTestApp::setup()
{
    loadFbo();
    loadShaders();
    loadBuffer();
    setupCamera();
    
    setLocalMatrix();
    
    //initialize texture and batch
    tex = gl::Texture::create(loadImage(loadAsset("ink.png")));
    mBatch = gl::Batch::create(mesh, shader,{
        {geom::CUSTOM_0,"previous"},
        {geom::CUSTOM_1,"next"},
        {geom::CUSTOM_2,"lineInfo"}
    });
}


void InkSpaceDrawTestApp::update()
{
    
    //on update we draw into the fbo
    gl::ScopedFramebuffer buff(canvas);{
        //gl::color(255, 0, 0);
        gl::setMatrices(mCam);
        
        gl::ScopedTextureBind tex0(tex,1);
        gl::ScopedGlslProg shd(shader);
        
        //miter isn't actually used
        //shader->uniform("miter", 0);
        shader->uniform("aspect", getWindowAspectRatio());
        shader->uniform("projection",drawCam.getProjectionMatrix());
        shader->uniform("modelview", localMatrix);
        shader->uniform("ink", 1);
        shader->uniform("focusPoint", -1400.0f);
        shader->uniform("drawTrans", 10.0f); //(int) ofGetElapsedTimef() % 6 > 3 ? 0.0 : 1.0);
        
        
        vector < vec3 > currentPoints;
        vector < vec3 > prevPositions;
        vector < vec3 > nextPositions;
        vector < vec3 > lineInfo;                 // could this be in one vec3?
        
        
        for (int i = 0; i < lines.size(); i++){
            addPolyline(lines[i],
                        currentPoints,
                        prevPositions,
                        nextPositions,
                        lineInfo,
                        true,
                        true,
                        1.0);
        }
        
        if (line.line.size() > 1){
            addPolyline(line,
                        currentPoints,
                        prevPositions,
                        nextPositions,
                        lineInfo,
                        true,
                        true,
                        1.0);
        }
        
        
        mesh->bufferAttrib(geom::POSITION, sizeof(vec3) * currentPoints.size(), currentPoints.data());
        mesh->bufferAttrib(geom::CUSTOM_0, sizeof(vec3) * prevPositions.size(), prevPositions.data());
        mesh->bufferAttrib(geom::CUSTOM_1, sizeof(vec3) * nextPositions.size(), nextPositions.data());
        mesh->bufferAttrib(geom::CUSTOM_2, sizeof(vec3) * lineInfo.size(), lineInfo.data());
        mBatch->draw();
        
    }
}

void InkSpaceDrawTestApp::draw()
{
    gl::clear( Color( 0, 0, 0 ) );
    gl::setMatricesWindow(getWindowSize());
    
    gl::draw(canvas->getColorTexture());
    
}

void InkSpaceDrawTestApp::mouseDown( MouseEvent event )
{
    auto pos = event.getPos();
    float x = pos.x;
    float y = pos.y;
    line.startDrawing();
    line.bAmDoneDrawing = false;
    line.line.addVertex(vec3(x,y,0));
    line.addedVertex();
    
}

void InkSpaceDrawTestApp::mouseUp( MouseEvent event )
{
    if (line.line.size() > 1){
        
        // skip ? ?f
        //line.line = line.line.getSmoothed(3);
        
        line.computeWidths();
        line.finishDrawing();
        line.bAmDoneDrawing = true;
        lines.push_back(line);
        line.bAmDoneDrawing = false;
    }
    line.line.clear();
    
}


void InkSpaceDrawTestApp::mouseDrag( MouseEvent event )
{
    auto pos = event.getPos();
    float x = pos.x;
    float y = pos.y;
    
    if (line.line.size() > 0){
        vec3 me(x,y,0);
        if (glm::length((me - line.line[line.line.size()-1])) > 3){
            line.line.addVertex(vec3(x,y,0));
            line.addedVertex();
            
            if (line.line.size() % 10 == 0 && line.line.size() > 20){
                
                int posA = (int)((line.line.size()-1) / 10) * 10;
                int posB = (int)((line.line.size() / 10)) * 10;
                moca::PolyLine3f b,c, d;
                splitPolylineAt(line.line, b, c, d, posA, posB);
                //d = d.getSmoothed(3);
                //d = d.getResampledBySpacing(2);
                //c.simplify(0.5);
                
                joinPolyline(line.line, b,c,d); //, <#ofPolyline &c#>)
            }
            
            // smooth near the front
            
            int sizeOfBlur = 10;
            if (line.line.size() > sizeOfBlur){
                
                int start = line.line.size()-sizeOfBlur;
                int end = line.line.size();
                
                vector < vec3 > pts;
                
                for (int i = start; i < end; i++){
                    float pct = (i-start) / (float)(sizeOfBlur - 1);
                    float amountOfBlur = sin(pct * M_PI);
                    
                    int i_m_1 = max(0, i-1);
                    int i_p_1 = fmin(line.line.size()-1, i+1);  // todo: replace size
                    
                    float pctMe = utils::map(amountOfBlur, 0, 1, 1.0, 0.6);
                    float pctThem = (1.0 - pctMe) / 2.0;
                    
                    vec3 pt =  (pctThem * line.line[i_m_1] +
                                pctMe * line.line[i] + pctThem * line.line[i_p_1]);
                    
                    pts.push_back(pt);
                    
                }
                
                for (int i = start; i < end; i++){
                    //cout << (i-start) << " ::::: " << line[i] << " ------ " << pts[i - start] << endl;
                    //line[i] = 0.8f * line[i]  + 0.2 * pts[i - start];
                    auto l = line.line[i];
                    l = pts[i - start];
                    //line.line[i] = pts[i-start];
                }
                
                
            }
            
            
            //line.line = line.line.getResampledBySpacing(3);
            //  line.line = line.line.getSmoothed(3);
            
        }
        line.computeWidths();
    }
}



CINDER_APP( InkSpaceDrawTestApp, RendererGl )
