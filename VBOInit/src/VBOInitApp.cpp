#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
using namespace ci;
using namespace ci::app;
using namespace std;

class VBOInitApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;
    
    
    ci::gl::VboRef vbo;
    ci::gl::VaoRef vao;
    ci::gl::VboMeshRef mesh;
    ci::gl::BatchRef mBatch;
    
    int numParticles = 10000;
    
    gl::GlslProgRef renderShader;
    GLint position;
};

void VBOInitApp::setup()
{
    
    //! BUILD DATA
    vector<ci::vec3> particles;
    for( int i = 0; i < numParticles; ++i )
    {
        particles.push_back(randVec3());
        
    }
    
     //! BUILD SHADER
    gl::GlslProg::Format fmt;
    fmt.vertex(loadAsset("shader.vert"));
    fmt.fragment(loadAsset("shader.frag"));
    
    renderShader = gl::GlslProg::create(fmt);

    
    //! CREATE VBO AND VAO
    vbo = gl::Vbo::create(GL_ARRAY_BUFFER,particles.size() * sizeof(ci::vec3),particles.data(),GL_STATIC_DRAW);
    vao = gl::Vao::create();
    
    
    //! GET ATTRIBUTE LOCATION FOR POSITION IN SHADER
    position = renderShader->getAttribLocation("ciPosition");
    
    //! bind vao to hold attributes
    gl::ScopedVao v(vao);
    
    //! bind buffer to link to attribute
    gl::ScopedBuffer buff(vbo);
    
    //! enable position attribute
    gl::enableVertexAttribArray(position);
    
    //! add pointer to the data describing it
    gl::vertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, 0);
 
}

void VBOInitApp::mouseDown( MouseEvent event )
{
}

void VBOInitApp::update()
{
}

void VBOInitApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
    gl::color(255, 0, 0);
    
    //bind shader
    gl::ScopedGlslProg prog(renderShader);
    
    //bind attributes
    gl::ScopedVao v(vao);

    //! draw all things
    gl::drawArrays(GL_TRIANGLES, 0,numParticles);
    
    
}

CINDER_APP( VBOInitApp, RendererGl )
