#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "PingPongBuffer.h"
#include "cinder/params/Params.h"

#include "cinder/Camera.h"

using namespace ci;
using namespace ci::app;
using namespace std;

/**
 *  This is a example to demonstrate primarily, one way on how to do
 *  GPU ping-ponging.
 *  
 *  The main example is built off of the RDiffusion sample in the samples folder included
 *  with the distribution. Essentially I broke off the ping-ponging into a seperate class. The implementaion does differ a little bit in that it's been re-written slightly
 *  to take advantage of C++11 lambda functions in order to customize the update loop of the buffer.
 */

class DiffusionApp : public App {
  public:
	void setup() override;
    void	keyDown( KeyEvent event ) override;
    static void prepareSettings(Settings * settings);
    void	mouseMove( MouseEvent event ) override;
    void	mouseDown( MouseEvent event ) override;
    void	mouseDrag( MouseEvent event ) override;
    void	mouseUp( MouseEvent event ) override;
	
    void update() override;
	void draw() override;
    vec2			mMouse;
    bool			mMousePressed;
    
    float			mReactionU;
    float			mReactionV;
    float			mReactionK;
    float			mReactionF;
    
    params::InterfaceGlRef	mParams;

    CameraPersp mCam;
    
    //! buffer for doing ping-ponging between textures
    PingPongBuffer buff;
    
    //! base texture to use in the process
    gl::TextureRef mTexture;
    
    //! shader for reaction diffusion process
    gl::GlslProgRef mRDShader;
    
    
};
void DiffusionApp::mouseDown( MouseEvent event )
{
    mMousePressed = true;
}

void DiffusionApp::mouseUp( MouseEvent event )
{
    mMousePressed = false;
}

void DiffusionApp::mouseMove( MouseEvent event )
{
    mMouse = event.getPos();
}

void DiffusionApp::mouseDrag( MouseEvent event )
{
    mMouse = event.getPos();
}
void DiffusionApp::prepareSettings(cinder::app::AppBase::Settings *settings){
  //  settings->setWindowSize(512, 512);
    settings->setFrameRate(60.0f);
}

void DiffusionApp::setup()
{
    mCam.lookAt( vec3( 0, 0, 2 ), vec3( 0 ) );
   
    mReactionU = 0.25f;
    mReactionV = 0.04f;
    mReactionK = 0.047f;
    mReactionF = 0.1f;
    
    // Setup the parameters
    mParams = params::InterfaceGl::create( "Parameters", ivec2( 175, 100 ) );
    mParams->addParam( "Reaction u", &mReactionU, "min=0.0 max=0.4 step=0.01 keyIncr=u keyDecr=U" );
    mParams->addParam( "Reaction v", &mReactionV, "min=0.0 max=0.4 step=0.01 keyIncr=v keyDecr=V" );
    mParams->addParam( "Reaction k", &mReactionK, "min=0.0 max=1.0 step=0.001 keyIncr=k keyDecr=K" );
    mParams->addParam( "Reaction f", &mReactionF, "min=0.0 max=1.0 step=0.001 keyIncr=f keyDecr=F" );

    
    mRDShader = gl::GlslProg::create( loadAsset("passThru_vert.glsl"), loadAsset( "gsrd_frag.glsl" ) );
    mTexture = gl::Texture::create( loadImage( loadAsset( "starter.jpg" ) ),
                                   gl::Texture::Format().wrap(GL_REPEAT).magFilter(GL_LINEAR).minFilter(GL_LINEAR) );
    
    gl::getStockShader( gl::ShaderDef().texture() )->bind();
    gl::ScopedTextureBind texBind( mTexture );
    buff.resetBuffers();
    
}


void DiffusionApp::keyDown( KeyEvent event )
{
     gl::ScopedTextureBind texBind( mTexture );
    if( event.getChar() == 'r' ) {
        buff.resetBuffers();
    }
}


void DiffusionApp::update()
{
    
    buff.update([=]()->void{
       
            gl::ScopedTextureBind texSrcBind( mTexture, 1 );
            gl::ScopedGlslProg shaderBind( mRDShader );
            
            mRDShader->uniform( "tex", 0 );
            mRDShader->uniform( "texSrc", 1 );
            mRDShader->uniform( "width", (float) 512 );
            mRDShader->uniform( "ru", mReactionU );
            mRDShader->uniform( "rv", mReactionV );
            mRDShader->uniform( "k", mReactionK );
            mRDShader->uniform( "f", mReactionF );
            
           
            gl::drawSolidRect( buff.getBounds() );
        
        if( mMousePressed ){
            gl::ScopedGlslProg shaderBind( gl::getStockShader( gl::ShaderDef().color() ) );
            gl::ScopedColor col( Color::white() );
            RectMapping windowToFBO( getWindowBounds(),buff.getCurrentBuffer()->getBounds() );
            gl::drawSolidCircle( windowToFBO.map( mMouse ), 25.0f, 32 );
        }
        
    });
    
   // system.updateDataTexture( buff.getCurrentBuffer()->getColorTexture());
}

void DiffusionApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
    gl::setMatricesWindow( getWindowSize() );
    gl::viewport( getWindowSize() );
    {
        gl::ScopedTextureBind bind( buff.getOutput());
        gl::drawSolidRect( getWindowBounds() );
    }
    
    gl::setMatrices(mCam);
  
    mParams->draw();
    
  

}

CINDER_APP( DiffusionApp, RendererGl, DiffusionApp::prepareSettings)
