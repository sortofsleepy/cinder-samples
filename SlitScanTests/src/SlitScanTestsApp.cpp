#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Capture.h"
#include "cinder/Log.h"
#include "cinder/ImageIo.h"
#include "cinder/Color.h"
#include "cinder/Rand.h"
using namespace ci;
using namespace ci::app;
using namespace std;

/**
 * A basic slit-scan example based on Golan Levin's Processing implementation
 * http://www.flong.com/storage/images/texts/slit_scan/processing/basic_slit_scan_live.pde
 *
 * Note : slight variation to the example in that I iterate over entire frame instead of just the height. 
 *
 *
 */

class SlitScanTestsApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
    void processSurface(Surface8uRef image);
	void draw() override;
    
    
    CaptureRef			mCapture;
    gl::TextureRef		mTexture;
    Surface8uRef output;
    
    int video_width = 640;
    int video_height = 480;
    int video_slice_x = video_width / 2;
    int window_width = 640;
    int window_height = 480;
    int draw_position_x = 640 - 1;
    
    bool newFrame = false;
    

};

void SlitScanTestsApp::processSurface(Surface8uRef image){
 
    if(newFrame){
    
        // convert a integer to a position in a grid
        // In Processing, the color of the pixels are stored in a flat array called pixels,
        // whereas in Cinder, there is no generalized array for getting all of the 
        // http://stackoverflow.com/questions/12402037/convert-index-to-grid-coordinate-mathematically
        for(int x = 0; x < video_width;++x){
            for(int y = 0; y < video_height; ++y){
                int setPixelIndex = y*window_width + draw_position_x;
                int getPixelIndex = y*video_width  + video_slice_x;
                
                int xCoord = setPixelIndex % video_width;
                int yCoord = setPixelIndex / video_width;
                
                int xGCoord = getPixelIndex % video_width;
                int yGCoord = getPixelIndex / video_width;
                
                
                output->setPixel(vec2(xCoord,yCoord), image->getPixel(vec2(xGCoord,yGCoord)));

               
            }
        }
        
        
        draw_position_x = (draw_position_x - 1);
        
        if(draw_position_x < 0){
         
            draw_position_x = app::getWindowWidth() - 1;
        }
        newFrame = false;
    }
}

void SlitScanTestsApp::setup()
{

    //initialize output Surface8u
    output = Surface::create(app::getWindowWidth(), app::getWindowHeight(), false);
  
    //initialize Capture object
    try {
        mCapture = Capture::create( video_width,video_height );
        mCapture->start();
    }
    catch( ci::Exception &exc ) {
        CI_LOG_EXCEPTION( "Failed to init capture ", exc );
    }

}

void SlitScanTestsApp::mouseDown( MouseEvent event )
{
}

void SlitScanTestsApp::update()
{
    
    //check for a new frame
    if( mCapture && mCapture->checkNewFrame() ) {
        newFrame = true;
        if( ! mTexture ) {
            // Capture images come back as top-down, and it's more efficient to keep them that way
            
            //process the frame
            processSurface(mCapture->getSurface());
            
            //initialize texture
            mTexture = gl::Texture::create(*output, gl::Texture::Format().loadTopDown());
            
        }
        else {
            //process the frame
            processSurface(mCapture->getSurface());
            
            //update texture
            mTexture->update(*output);
        }
    }
}

void SlitScanTestsApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
 
    //draw if initialized
    if( mTexture ) {
        gl::ScopedModelMatrix modelScope;
        gl::draw( mTexture );
        
    }
}

CINDER_APP( SlitScanTestsApp, RendererGl )
