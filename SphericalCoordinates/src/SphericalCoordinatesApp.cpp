#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "SphereSystem.h"
#include "cinder/Camera.h"
using namespace ci;
using namespace ci::app;
using namespace std;

class SphericalCoordinatesApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;
    CameraPersp mCam;
    SphereSystem system;
};

void SphericalCoordinatesApp::setup()
{
    mCam.lookAt( ci::vec3( 0.0f, 0.0f, 1000.0f) ,  ci::vec3( 0.0f, 0.0f, 0.0f ) );
    mCam.setPerspective( 75.0, getWindowAspectRatio(), 1, 10000 );
    
    
    system.setup();
}

void SphericalCoordinatesApp::mouseDown( MouseEvent event )
{
}

void SphericalCoordinatesApp::update()
{
    system.update();
}

void SphericalCoordinatesApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
    gl::setMatrices(mCam);
    system.draw();
}

CINDER_APP( SphericalCoordinatesApp, RendererGl, [] ( App::Settings *settings ) {
    settings->setWindowSize( 1280, 720 );
    settings->setMultiTouchEnabled( false );
} )

