//
//  SphereSystem.cpp
//  SphericalCoordinates
//
//  Created by Joseph Chow on 12/9/15.
//
//

#include "SphereSystem.h"
#include "cinder/Rand.h"
using namespace ci;
using namespace app;
using namespace std;

SphereSystem::SphereSystem(){}
void SphereSystem::setup(){
    
    thetaSpeed = randFloat(-0.01,1.0);
    phiSpeed = randFloat(-0.01,1.0);

    
    // Create initial particle layout.
    vector<Particle> particles;
    particles.assign( NUM_PARTICLES, Particle() );
    const float azimuth = 256.0f * M_PI / particles.size();
    const float inclination = M_PI / particles.size();
    const float radius = 180.0f;
    vec3 center = vec3( getWindowCenter() + vec2( 180.0f, 40.0f ), 0.0f );
    for( int i = 0; i < particles.size(); ++i )
    {	// assign starting values to particles.
        float x = radius * sin( inclination * i ) * cos( azimuth * i );
        float y = radius * cos( inclination * i );
        float z = radius * sin( inclination * i ) * sin( azimuth * i );
        
        auto &p = particles.at( i );
        p.pos = center + vec3( randFloat(x), randFloat(y), z);
        p.home = p.pos;
        p.ppos = p.home + Rand::randVec3() * 10.0f; // random initial velocity
        p.color = Color( CM_HSV, lmap<float>( i, 0.0f, particles.size(), 0.0f, 0.66f ), 1.0f, 1.0f );
        p.phi = 0;
        p.theta = 0;
        p.thetaSpeed = randFloat(-0.01,1.0);
        p.phiSpeed = randFloat(-0.01,1.0);
    }
    
    ci::gl::VboMeshRef tea = gl::VboMesh::create(geom::Teapot().subdivisions(7));
    ci::gl::VboRef m  = tea->getVertexArrayLayoutVbos().at(0).second;
    
    // Create particle buffers on GPU and copy data into the first buffer.
    // Mark as static since we only write from the CPU once.
    mParticleBuffer[mSourceIndex] = gl::Vbo::create( GL_ARRAY_BUFFER, particles.size() * sizeof(Particle), particles.data(), GL_STATIC_DRAW );
    mParticleBuffer[mDestinationIndex] = gl::Vbo::create( GL_ARRAY_BUFFER, particles.size() * sizeof(Particle), nullptr, GL_STATIC_DRAW );
    
    // Create a default color shader.
    mRenderProg = gl::getStockShader( gl::ShaderDef().color() );
    
    for( int i = 0; i < 2; ++i )
    {	// Describe the particle layout for OpenGL.
        mAttributes[i] = gl::Vao::create();
        gl::ScopedVao vao( mAttributes[i] );
        
        // Define attributes as offsets into the bound particle buffer
        gl::ScopedBuffer buffer( mParticleBuffer[i] );
        gl::enableVertexAttribArray( 0 );
        gl::enableVertexAttribArray( 1 );
        gl::enableVertexAttribArray( 2 );
        gl::enableVertexAttribArray( 3 );
        gl::enableVertexAttribArray( 4 );
        gl::enableVertexAttribArray( 5 );
        gl::enableVertexAttribArray( 6 );
        gl::enableVertexAttribArray( 7 );
        
        gl::vertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, pos) );
        gl::vertexAttribPointer( 1, 4, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, color) );
        gl::vertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, ppos) );
        gl::vertexAttribPointer( 3, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, home) );
        gl::vertexAttribPointer( 4, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, theta) );
        gl::vertexAttribPointer( 5, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, phi) );
        gl::vertexAttribPointer( 6, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, thetaSpeed) );
        gl::vertexAttribPointer( 7, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)offsetof(Particle, phiSpeed) );
        
    }
    
    gl::GlslProg::Format updateFmt;
    updateFmt.vertex(app::loadAsset("particleUpdate.vs"));
    updateFmt.feedbackFormat( GL_INTERLEAVED_ATTRIBS );
    updateFmt.feedbackVaryings( { "position", "pposition", "home", "color", "theta","phi","thetaSpeed","phiSpeed" } );
    updateFmt.attribLocation( "iPosition", 0 )
    .attribLocation( "iColor", 1 )
    .attribLocation( "iPPosition", 2 )
    .attribLocation( "iHome", 3 )
    .attribLocation( "iTheta", 4 )
    .attribLocation( "iPhi", 5)
    .attribLocation( "iThetaSpeed", 6)
    .attribLocation( "iPhiSpeed", 7);
    
    mUpdateProg = gl::GlslProg::create(updateFmt);
    
}

void SphereSystem::update(){
    // Update particles on the GPU
    gl::ScopedGlslProg prog( mUpdateProg );
    gl::ScopedState rasterizer( GL_RASTERIZER_DISCARD, true );	// turn off fragment stage
    mUpdateProg->uniform( "uMouseForce", mMouseForce );
    mUpdateProg->uniform( "uMousePos", mMousePos );
    
    
    
    // Bind the source data (Attributes refer to specific buffers).
    gl::ScopedVao source( mAttributes[mSourceIndex] );
    // Bind destination as buffer base.
    gl::bindBufferBase( GL_TRANSFORM_FEEDBACK_BUFFER, 0, mParticleBuffer[mDestinationIndex] );
    gl::beginTransformFeedback( GL_POINTS );
    
    // Draw source into destination, performing our vertex transformations.
    gl::drawArrays( GL_POINTS, 0, NUM_PARTICLES );
    
    gl::endTransformFeedback();
    
    // Swap source and destination for next loop
    std::swap( mSourceIndex, mDestinationIndex );
    
    // Update mouse force.
    if( mMouseDown ) {
        mMouseForce = 150.0f;
    }

}

void SphereSystem::draw(){

    gl::enableDepthRead();
    gl::enableDepthWrite();

    gl::ScopedGlslProg render( mRenderProg );
    gl::ScopedVao vao( mAttributes[mSourceIndex] );
    gl::context()->setDefaultShaderVars();
    gl::drawArrays( GL_POINTS, 0, NUM_PARTICLES );
}