//
//  FeedbackBuffer.h
//  InterleaveTest
//
//  Created by Joseph Chow on 10/25/15.
//
//

#ifndef __InterleaveTest__FeedbackBuffer__
#define __InterleaveTest__FeedbackBuffer__

#include "cinder/gl/Vbo.h"
#include "cinder/gl/TransformFeedbackObj.h"


/**
 *  A class to (hopefully) make it a bit simpler to use Transform feedback.
 *  Designed to either be subclassed or used on it's own.
 */
class FeedbackBuffer {
    
    //! buffers for ping-ponging
    ci::gl::VboRef buffers[2];
    
    //! vaos to hold attributes;
    ci::gl::VaoRef vaos[2];
    
    //! transform feedback object for bouncing sets of vbos
    ci::gl::TransformFeedbackObjRef feedbackObjs[2];
    
    
protected:
    //! The geometry type for doing transform feedback
    GLenum drawType = GL_POINTS;
    
    //! Indicates how your data is formed. Data is considered seperate by default
    GLenum dataStructureType = GL_SEPARATE_ATTRIBS;
    
    //! acts as a flag to indicate which in the set of buffers and vaos we're on
    uint32_t mDrawBuff = 1;
    
    int numItems = 0;
    
public:
    FeedbackBuffer();
    
    
    //! shader used for updating
    ci::gl::GlslProgRef updateShader;
    
    //! shader used for rendering.
    ci::gl::GlslProgRef renderShader;
    
    //! retrives bothe of the buffers
    ci::gl::VboRef* getBuffers();
    
    ci::gl::VboRef getBuffer(int index);
    
    void bind();
    void unbind();
    
    //! returns the current buffer we're drawing to
    ci::gl::VboRef getDrawBuffer(){
        return buffers[1-mDrawBuff];
    }
    
    //! updates things, ping-pongs buffers. Accepts a callback function for any additional processing
    void update(const std::function<void()> fn);
    
    //! sets the type of structure your attributes should be considered in
    void setDataStructureType(GLenum dataStructureType=GL_SEPARATE_ATTRIBS);
    
    //! sets the geometry type used in transform feedback
    void setGeometryType(GLenum drawType=GL_POINTS);
    
    //! Applies data you set onto a buffer.
    //! Expects a callback function for enabling and pointing to vertex attributes.
    //! The structure of your data can be interleaved or not depending on what you're building.
    template<typename T>
    void setData(typename std::vector<T> &data,std::function<void()> fn){
        buffers[0] = ci::gl::Vbo::create( GL_ARRAY_BUFFER, data.size() * sizeof(T), data.data(), GL_STATIC_DRAW );
        
        // Create another Position Buffer that is null, for ping-ponging
        buffers[1] = ci::gl::Vbo::create( GL_ARRAY_BUFFER, data.size() * sizeof(T), nullptr, GL_STATIC_DRAW );
        
        numItems = data.size();
        for( int i = 0; i < 2; i++ ) {
            // Initialize the Vao's holding the info for each buffer
            vaos[i] = ci::gl::Vao::create();
            
            vaos[i]->bind();
            buffers[i]->bind();
            fn();
        }

    }
    
    //! Works similarly to the first form of FeedbackBuffer::setData but requires you to initialize a
    //! main ci::gl::VboRef yourself. Accepts the buffer as a param along with the same callback function
    void setData(ci::gl::VboRef buffer,int dataSize, std::function<void()> fn);
    

    void setUpdateShader(ci::gl::GlslProgRef updateShader);
    void setRenderShader(ci::gl::GlslProgRef renderShader);
};

#endif /* defined(__InterleaveTest__FeedbackBuffer__) */
