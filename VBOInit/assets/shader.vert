#version 150

uniform mat4	ciModelViewProjection;
uniform mat3	ciNormalMatrix;

in vec3		ciPosition;
in vec2		ciTexCoord0;
in vec3		ciNormal;
in vec4		ciColor;

uniform float time;

void main( void )
{
   
    //vec4 pos = ciModelViewProjection * ( 4.0 * vec4(ciPosition ,1.0) + vec4( vInstancePosition, 0 ) );
    vec4 pos = vec4(ciPosition,1.0);
    gl_Position	= pos;
   
}
