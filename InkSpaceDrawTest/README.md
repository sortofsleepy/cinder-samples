### InkSpace drawing test ###

This was just my attempt at porting over the drawing portion of
Zach Lieberman's Android experiment [InkSpace](https://github.com/ofZach/inkSpace)
in an effort to better understand one way of drawing lines in OpenGL as well as
to try and put together a foundation for a silly drawing app idea of my own
that could have similar mechanics.

The code is mostly untouched, minus the swaping out of [openFrameworks](http://openframeworks.cc/) specific stuff

Included also is a altered PolyLine class. It's basically the same as Cinder's but includes an operator function
to interact with the vector of points for the line, as well as a function to completely clear the line. 
The class is namespaced under the namespace name "moca" to prevent confusion with the class under the cinder namespace.


### Credit ###
[Zach Lieberman](http://thesystemis.com/)