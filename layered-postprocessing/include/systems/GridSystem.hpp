#ifndef GridSystem_hpp
#define GridSystem_hpp
#include "Object3D.hpp"
#include <memory>

typedef std::shared_ptr<class GridSystem> GridSystemRef;

class GridSystem : public Object3D {
public:
    GridSystem(int width,int height, int resolution);
    static GridSystemRef create(int width=640,int height=480,int resolution=8){
        return GridSystemRef(new GridSystem(width,height,resolution));
    }
};

#endif
