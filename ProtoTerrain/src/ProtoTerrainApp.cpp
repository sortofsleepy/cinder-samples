#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "moca/core/includes.h"
#include "Terrain.hpp"
#include "cinder/CameraUi.h"
using namespace ci;
using namespace ci::app;
using namespace std;
// width and height of our mesh
static const int kWidth = 1024;
static const int kHeight = 1024;

class ProtoTerrainApp : public App {
  public:
	void setup() override;
    
    void mouseDown( MouseEvent event ) override;
    void mouseDrag( MouseEvent event ) override;
    void mouseWheel( MouseEvent event ) override;
    
	void update() override;
	void draw() override;
    
    
    CameraPersp mCam;
    CameraUi mCamUi;
    
    Terrain land;
};

void ProtoTerrainApp::setup()
{
    //mCam.lookAt( vec3( 0, 0, 50 ), vec3( 0 ) );
    //mCamUi.setCamera(&mCam);
    
    mCam.setPerspective( 50.0f, 1.0f, 1.0f, 10000.0f );
    mCam.lookAt( vec3( -kWidth / 4, kHeight / 2, -kWidth / 8 ), vec3( kWidth / 4, -kHeight / 8, kWidth / 4 ) );
    
    mCamUi.setCamera( &mCam );
  
}


void ProtoTerrainApp::mouseWheel( MouseEvent event )
{
    
    mCamUi.mouseWheel(event);
}
void ProtoTerrainApp::mouseDown( MouseEvent event )
{
    
    mCamUi.mouseDown( event.getPos() );
}
void ProtoTerrainApp::mouseDrag( MouseEvent event )
{
    
    mCamUi.mouseDrag( event.getPos(), event.isLeftDown(), event.isMiddleDown(), event.isRightDown() );
}
void ProtoTerrainApp::update()
{
    land.updateNoise();
}

void ProtoTerrainApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
    gl::setMatrices(mCam);
    gl::ScopedDepth depth(true);
    land.draw();
}

CINDER_APP( ProtoTerrainApp, RendererGl, [] ( App::Settings *settings ) {
    settings->setWindowSize( 1280, 720 );
    settings->setMultiTouchEnabled( false );
} )
