//
//  SphereSystem.h
//  SphericalCoordinates
//
//  Created by Joseph Chow on 12/9/15.
//
//

#ifndef __SphericalCoordinates__SphereSystem__
#define __SphericalCoordinates__SphereSystem__

#include <stdio.h>

/**
 Particle type holds information for rendering and simulation.
 Used to buffer initial simulation values.
 */
struct Particle
{
    ci::vec3	pos;
    ci::vec3	ppos;
    ci:: vec3	home;
    ci::ColorA  color;
    float theta;
    float phi;
    float thetaSpeed;
    float phiSpeed;
};


const int NUM_PARTICLES = 300;

class SphereSystem {
    ci::gl::GlslProgRef mRenderProg;
    ci::gl::GlslProgRef mUpdateProg;
    
    // Descriptions of particle data layout.
    ci::gl::VaoRef		mAttributes[2];
    // Buffers holding raw particle data on GPU.
    ci::gl::VboRef		mParticleBuffer[2];
    
    // Current source and destination buffers for transform feedback.
    // Source and destination are swapped each frame after update.
    std::uint32_t	mSourceIndex		= 0;
    std::uint32_t	mDestinationIndex	= 1;
    
    // Mouse state suitable for passing as uniforms to update program
    bool			mMouseDown = false;
    float			mMouseForce = 0.0f;
    ci::vec3			mMousePos = ci::vec3( 0, 0, 0 );

    
    float thetaSpeed;
    float phiSpeed;
public:
    SphereSystem();
    void setup();
    void update();
    void draw();
};
#endif /* defined(__SphericalCoordinates__SphereSystem__) */
