__InstanceTesting__

This project demonstrates one way to do Instanced drawing of a `VboMesh`. Another goal of the example
is to demonstrate how to upload data onto a `VboRef` as, unlike a `VboMeshRef`, you can't simply call `->bufferAttrib` etc.

It's based on the [InstancedTeapots](https://github.com/cinder/Cinder/tree/master/samples/_opengl/InstancedTeapots) example from the samples included with Cinder.


It should draw a multi-colored grid of teapots.
Code is commented. 