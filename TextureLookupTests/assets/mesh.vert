#version 150

uniform mat4	ciModelViewProjection;
uniform mat3	ciNormalMatrix;

in vec4		ciPosition;
out vec4 position;

void main( void )
{
    vec4 pos = ciModelViewProjection * ciPosition;
    position = pos;
    gl_Position	= pos;
   
}
