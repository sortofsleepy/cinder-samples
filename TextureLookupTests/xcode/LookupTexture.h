//
//  LookupTexture.h
//  TextureLookupTests
//
//  Created by Joseph Chow on 11/9/15.
//
//

#ifndef __TextureLookupTests__LookupTexture__
#define __TextureLookupTests__LookupTexture__

#include "cinder/gl/Texture.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/Shader.h"
#define STRINGIFY(A) #A

/**
 *  A simple class trying to make things a little bit easier when 
 *  trying to use textures for data storage.
 */
class LookupTexture {
   
    //! Fbo we're gonna be writing to
    ci::gl::FboRef buffer;
    
    //! the shader that will conduct the lookup
    ci::gl::GlslProgRef lookupShader;
    
    //! the texture used to store our lookup data
    ci::gl::TextureRef lookupTexture;
    
    //! fragment shader string for lookup
    std::string fragShader;
    
    //! vertex shader string for lookup
    std::string vertShader;
    
    //! resolution of our grid of data
    int resolution = 1.0;
    
public:
    
    //! default width of fbo
    int width = 1024;
    
    //! default height of fbo
    int height = 1024;
    
    //! constructors. One accepts a alternate resolution.
    LookupTexture();
    LookupTexture(int resolution);
    
    
    //! returns the lookup buffer
    ci::gl::FboRef getBuffer(){
        return buffer;
    }
    
    //! loads the shaders that handle the lookup.
    //! shaders are inline with the class.
    void loadShader();
    
    //! Sets up the LookupTable. Pass in your Surface32f data source
    //! as the parameter.
    void setup(ci::Surface32f data);
    
    //! Looks up a position in the table and draws the result to the fbo.
    void lookupPosition(ci::vec2 position);
    
    //! Returns the current result being displayed in the buffer as a texture
    ci::gl::TextureRef getPosition();
    
    //! Returns the current result of the lookup after running a lookup call.
    ci::gl::TextureRef getPosition(ci::vec2 position);
};
#endif /* defined(__TextureLookupTests__LookupTexture__) */
